#ifndef __MAIN_H_
#define __MAIN_H_

#include "STC8H.H"
#include "intrins.h"
#include "stdio.h"
#include "sys.h"

#define FOSC            38000000UL


#define LED1 P34
#define LED2 P17

#define KEY1 P37
#define KEY2 P54

void Timer0Init(void);
extern uint32_t timer;

void UartInit(void);
void usrt_send_buf(uint8_t *buf, uint8_t len);

void bldc_init();
void pwm_set(uint8_t pwm);
void bldc_deal();

void key_deal();

void led_init();
void set_led1(uint16_t pwm);
void set_led2(uint16_t pwm);

#endif

