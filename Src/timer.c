#include "main.h"

#define TIMER_FREQ 1000
#define BRT        (65536 - FOSC / TIMER_FREQ)

uint32_t timer=0;

void Timer0Init(void)
{
	AUXR |= 0x80;		//定时器时钟1T模式
	TMOD &= 0xF0;		//设置定时器模式
	TL0 = BRT;		//设置定时初始值
	TH0 = BRT>>8;   //设置定时初始值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时
	
	ET0=1;
}

/*----------------------------
定时器0中断服务程序
----------------------------*/
void timer0() interrupt 1
{
	timer++;
//	freq_deal();
}


