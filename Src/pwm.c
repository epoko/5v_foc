#include "main.h"

#define PWM2_PSCR_DIV 1
#define PWM2_COUNT_MAX 65536


void led_init()
{
    P_SW2 = 0x80;
    
    PWMB_PS = (0x1 << 0) | (0x1 << 6);
    
    PWM2_ENO = 0;
    PWM2_CCER1 = 0; //close channel
    PWM2_CCER2 = 0; //close channel
    
    PWM2_CCMR1 = (0 << 7)|(0x6 << 4)|(1 << 3)|(0 << 2)|(0x0 << 0);  //set mode
    PWM2_CCMR4 = (0 << 7)|(0x6 << 4)|(1 << 3)|(0 << 2)|(0x0 << 0);  //set mode
    
    PWM2_PSCR = PWM2_PSCR_DIV-1;  //ser pscr
    PWM2_ARR = PWM2_COUNT_MAX-1;   //set arr
    
    PWM2_CCR1 = 0;
    PWM2_CCR4 = 0;
    
    PWM2_CCER1 = Bin(00000001); //out mode
    PWM2_CCER2 = Bin(00010000); //out mode
    PWM2_ENO = Bin(01000000);   //out enable
    PWM2_BKR = 0x80;    //disale bkr
    
    PWM2_CR1 = 0x01; //start counter
}


void set_led1(uint16_t pwm)
{
    PWM2_CCR4 = pwm;
}

void set_led2(uint16_t pwm)
{
    PWM2_CCR1 = pwm;
}
