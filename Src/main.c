#include "main.h"


void main()
{
    LED1 = 0;
    LED2 = 0;
    P1M0 |= Bin(10000000);
    P1M1 &= ~Bin(10000000);
    P3M0 |= Bin(00010000);
    P3M1 &= ~Bin(00010000);
    
    KEY1 = 1;
    KEY2 = 1;
    P3M0 &= ~Bin(10000000);
    P3M1 &= ~Bin(10000000);
    P5M0 &= ~Bin(00010000);
    P5M1 &= ~Bin(00010000);
    
    
    Timer0Init();
    UartInit();
    bldc_init();
    led_init();
	EA = 1;
    
    printf("system start!\n");
    
    while(1)
    {
        key_deal();
        bldc_deal();
    }
}

